var User = require('../../models/user');


var getProfile = function(req, res){
    User.findById(req.user, function(err, user){
        if(err) throw err;
        res.json(user.profile);
    });
};

var getProfileById = function(req, res, userId){
    User.findById(userId, function(err, user){
      //  if(err) throw err;
        if(!user){
          //TODO: check out better solution for not found data
          res.json({error: 'No Such user'});
        }else{
          res.json(user.profile);
        }

    });
};

module.exports = {
  getProfile: getProfile,
  getProfileById: getProfileById,
};

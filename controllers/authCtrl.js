var authHelper = require('../helpers/auth');
var passport = require('passport');


const registerViewPath = 'auth/register';
const loginViewPath = 'auth/login';
const indexPath ='home.ejs';



var registerView = function(req,res){
   res.render(registerViewPath, {title: 'Register', csrfToken: req.csrfToken()});
};
var loginView = function(req,res){
   res.render(loginViewPath, { csrfToken: req.csrfToken(), errors: req.flash('loginMessage') });
};
var index = function(req,res){
  //console.log(req.user);
  //console.log(req.isAuthenticated());
   res.render(indexPath);
};



var register = function(req, res){
  //validate register form
  var errors = authHelper.checkRegister(req);
  if(errors){
   console.log("errors", errors);
    res.render(registerViewPath, {title: 'Register Errors', errors: errors, csrfToken: req.csrfToken()});
  }else{
    const email = req.body.email;
    const password = req.body.password;
    //save new user and login
    authHelper.signupUser(email, password,function(newUser){
      req.login(newUser,function(err){
        if(err) throw err;
        //console.log("user Id: ",newUser._id);
        res.redirect('/');
      });
    });
  }

};

//if pass redirect to /profile, if nor go to /login
// authentication can be found under ./config/passport
var loginAuth =  passport.authenticate('local-login',
  {
    successRedirect: '/user/profile',
    failureRedirect: '/login',
    failureFlash : true // allow flash messages
  }
);



var logout = function(req, res){
  req.logout();
  req.session.destroy(() => {
       res.clearCookie('connect.sid')
       res.redirect('/')
   })
};



module.exports = {
    registerView : registerView,
    register: register,
    loginView: loginView,
    loginAuth: loginAuth,
    index: index,
    logout: logout,
  }

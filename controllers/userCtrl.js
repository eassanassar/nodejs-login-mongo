var User = require('../models/user');
const profileViewPath = 'user/profile';


var findByEmail = function(email, onComplete){
    User.findOne({'local.email': email}, function(err, user){
    //  if(err) throw user;
      onComplete(err, user);
    });
};

var profileView = function(req,res){
   res.render(profileViewPath);
};


module.exports = {
  findByEmail: findByEmail,
  profileView: profileView
};

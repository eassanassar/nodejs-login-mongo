const mongoose = require('mongoose');
const configDB = require('./config/database.js');

mongoose.Promise = global.Promise;

module.exports = function(){
  mongoose.connect(configDB.url,{useMongoClient: true});
  //test connection
  mongoose.connection.once('open', function(){
      console.log('mongoDB connected sucessfully !!');
  }).on('error', function(error){
      console.log('MongoDB Connection error: ', error);
  });
}

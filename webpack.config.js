module.exports = {
  //define entry point
  entry: './js/script-1.js',

  //define output point
  output:{
    //the path is the directory you want it to be in , if it does not exist webpack will make it
    path: __dirname + '/public/js/',
    filename:'bundle.js'
  },

  module:{
    loaders:[
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: "babel-loader",
        query:{
          presets:['es2015']
        }
      },
      {
          test: /\.scss$/,
          loader: "style-loader!css-loader!sass-loader",
      }
    ]
  }

}
// to run it just write webpack in the console

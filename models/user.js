const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');

//create Schema and model

const userSchema = new Schema({
  local : {
        email: {type: String, required: true, lowercase: true, unique: true},
        password: {type: String, required: true}
  },
  profile: {
    name: {type: String, default: ''},
    picture: {type: String, default: ''}
  }

});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

const  User = mongoose.model('user', userSchema);

module.exports = User;

require('dotenv').config()
var express = require('express');
//loggin npm
var morgan = require('morgan');
var port     = process.env.PORT || 8080;
var loginValidate = require('./helpers/auth').loginValidate;
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');

//Authintication Packages
var session = require('express-session');
var cookieParser = require('cookie-parser');
var MongoStore = require('connect-mongo')(session);
var passport = require('passport');
var flash    = require('connect-flash');

var csrf = require('csurf')

const configDB = require('./config/database.js');
// connect to moongose
require('./db')();
require('./config/passport')(passport);

var app = express();
var allRoutes = require('./routes/allRoutes');



//set up template engine
app.set('view engine', 'ejs');
//app.set('views',  __dirname +'\\views');

app.use(morgan('dev'));
//static files
app.use(express.static('./public'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());
app.use(cookieParser()); // read cookies (needed for auth)
app.use(session({
    secret: 'iameassanassaraeassanassar-1525FF',
    //save even if unchanged
    resave: true,
    saveUninitialized: false,
    store: new MongoStore({ url: configDB.url}),
    //cookie: { expires : 0 },

}));
app.use(passport.initialize());
app.use(passport.session());
//app.use(csrf({cookie: true}));
app.use(csrf());
app.use(flash());


//storing variables for the specific request/response cycle. in res.locals the variables are only available to the view associated with the response.
app.use(function(req, res, next){
// we need this so the in the ejs we know if connected or not
  res.locals.isLoggedIn = req.isAuthenticated();
//  var token = req.csrfToken();
//  res.cookie('XSRF-TOKEN', token);
  return next();
});


//fire routes

allRoutes(app, express);

//app.use('/', authRoute);
//app.use('/user', userRoute);

// Handle 404 - must Keep this as a last route
app.use(function(req, res, next) {
    res.status(404);
    res.redirect('/');
});



app.listen(port);
console.log(`app is listening to port ${port}`);

var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');

module.exports = function(passport) {

  passport.serializeUser(function(user, done){
    //when logged in it will serielize the user object and latter on be passed to be deserialize
    return done(null, user._id);
  });

  passport.deserializeUser(function(userId, done){
  //  User.findById(id, function(err, user){
      // req.user in any request will show you this user
      return done(null, userId);
  //  });

  });


  passport.use('local-login',new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true
    },
   function(req, email, password, done) {
     email = email.toLowerCase();
     console.log("email: ", email);
     // find a user whose email is the same as the forms email
     // we are checking to see if the user trying to login already exists
     User.findOne({ 'local.email' :  email }, function(err, user) {
         // if there are any errors, return the error before anything else
         if (err){
             return done(err);
         }
         // if no user is found, return the message
         else if (!user){
           //  return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash

           return done(null, false, req.flash('loginMessage', 'the username or password is incorrect'));
         }
         else if (user.validPassword(password)){
           // all is well, return successful user
           //console.log(user);
           return done(null, user);
         }else{
           // if the user is found but the password is wrong
           // create the loginMessage and save it to session as flashdata
           //return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));
           return done(null, false, req.flash('loginMessage', 'the username or password is incorrect'));
         }

     });

    }
  ));
}

var User = require('../models/user');
var UserCtrl = require('../controllers/userCtrl');



//validate register form
var checkRegister = function(req){
  //console.log("ddddddddd");
  //TODO: check if email is also unique
  //TODO: check password strength

  req.checkBody({
    'email': {
      notEmpty: {
        errorMessage: 'email is required and can\'t be empty'
      },
      isEmail:{
         errorMessage: 'Invalid Email Address'
      },

    },
    'password': {
      notEmpty:{
          errorMessage: 'password is required and can\'t be empty'
      },
    },
    'passwordMatch':{
        notEmpty: {
          errorMessage:'password is required and can\'t be empty'
        }
    }

  });

  req.checkBody('email', 'email address must be between 4-100 charechters ').len(4,100);
  req.checkBody('password','password must be between 8-100 charechters').len(8,100);
  req.checkBody('passwordMatch', 'password do not match, please try again').equals(req.body.password);

  /*
    ^                         Start anchor
    (?=.*[A-Z].*[A-Z])        Ensure string has two uppercase letters.
    (?=.*[!@#$&*])            Ensure string has one special case letter.a
    (?=.*[0-9].*[0-9])        Ensure string has two digits.
    (?=.*[a-z].*[a-z].*[a-z]) Ensure string has three lowercase letters.
    .{8}                      Ensure string is of length 8.
    $                         End anchor.
  */
  //req.checkBody('password','password must have two uppercase letters.').matches(/^(?=.*[A-Z].*[A-Z]) $/,"i");
  //req.checkBody('password','password must have one special case letter').matches(/^(?=.*[!@#$&*])$/,"i");
  //req.checkBody('password','password must have two digits').matches(/^(?=.*[0-9].*[0-9])$/,"i");
  //req.checkBody('password','password must have three lowercase letters.').matches(/^(?=.*[a-z].*[a-z].*[a-z])$/,"i");
//  req.checkBody('password','password must include one lowercase charechter, one uppercase charecter, a number, and a specialcharecter.').matches(/^(?=.*[A-Z].*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z].*[a-z]).{8}$/);

  return req.validationErrors();
};

var signupUser = function(email, password, onSuccess){

    var newUser = new User();
    newUser.local.email = email;
    newUser.local.password = newUser.generateHash(password);
    newUser.save(function(err){
      if(err){
        throw err;
      }else{
        onSuccess(newUser);
      }
    });
};


module.exports = {
  checkRegister: checkRegister,
  signupUser: signupUser
}

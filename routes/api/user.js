var userApi = require('../../controllers/api/userApi');
var globals = require('../../helpers/globals');


module.exports = function(app, express){
  var router = express.Router();

  router.get('/profile', globals.isLoggedIn, function(req, res){ userApi.getProfile(req, res)});
  //TODO: might need to set limitaion to what data wanted to get later on
  router.get('/profile/:id', globals.isLoggedIn, function(req, res){ userApi.getProfileById(req, res, req.params.id)});

  app.use('/api/user', router);
}

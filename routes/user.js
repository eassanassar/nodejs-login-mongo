var userCtrl = require('../controllers/userCtrl');
var globals = require('../helpers/globals');


module.exports = function(app, express){
  var router = express.Router();
  router.get('/profile', globals.isLoggedIn, function(req, res){ userCtrl.profileView(req, res)});

  app.use('/user', router);
}

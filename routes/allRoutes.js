var authRoute = require('./auth');
var userRoute = require('./user');

/*API*/
var userApiRoute = require('./api/user');


module.exports= function(app, express){
  authRoute(app, express);
  userRoute(app, express);

  /* API */
  userApiRoute(app, express);
}

var authCtrl = require('../controllers/authCtrl');


module.exports = function(app, express){
  var router = express.Router();

  router.get('/', function(req, res){ authCtrl.index(req, res)});
  router.get('/register',function(req, res){   authCtrl.registerView(req, res); });
  router.post('/register',  function(req, res){  authCtrl.register(req, res);});
  router.get('/login', function(req, res){ authCtrl.loginView(req, res)});
  router.post('/login',  authCtrl.loginAuth);
  router.get('/logout', function(req, res){ authCtrl.logout(req, res)});

  app.use('/', router);

}

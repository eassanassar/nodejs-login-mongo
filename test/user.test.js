// to define test change "test: ..." in package.json to  test: "mocha"
// to run it in the cmd write : npm run test
const assert = require('assert');
const User = require('../models/user');
const authHelper = require('../helpers/auth');

const email = "contact@eassaworld.com";
const password = "P@ssW0rdTest"
var newUser = {};

describe('User record', function(){

  after(function(done) {
    newUser.remove();
    done();
  });

  it('signup User record to the database', function(done){

    authHelper.signupUser(email, password,function(user){
      newUser = user;
      // if saved to the database isNew = false
      assert(!user.isNew);

      done();
    });
  });

  it('find local User by id',function(done){
      User.findOne({_id :newUser._id.toString()},function(err, user){
        assert(user.local.email === email);
        done();
      });
  });

  it('find user validate password',function(done){
    User.findOne({ 'local.email' :  email }, function(err, user) {
        assert(user.validPassword(password));
        done();
      });
  });


});

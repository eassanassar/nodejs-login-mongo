const mongoose = require('mongoose');
const configDB = require('../config/database.js');

// ES6 Promises
mongoose.Promise = global.Promise;

// Connect to db before tests run
before(function(done){

    // Connect to mongodb
    mongoose.connect(configDB.url,{useMongoClient: true});
    mongoose.connection.once('open', function(){
        console.log('mongoDB connected sucessfully .....');
        done();

    }).on('error', function(error){
        console.log('Connection error:', error);
        done(error);
    });

});
/*
// Drop the characters collection before each test
beforeEach(function(done){
    // Drop the collection
    mongoose.connection.collections.users.drop(function(){
        done();
    });
});
*/
